<?php

namespace App\Http\Controllers;

use App\Models\Pari;
use Illuminate\Http\Request;

class PariController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pari  $pari
     * @return \Illuminate\Http\Response
     */
    public function show(Pari $pari)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pari  $pari
     * @return \Illuminate\Http\Response
     */
    public function edit(Pari $pari)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pari  $pari
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pari $pari)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pari  $pari
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pari $pari)
    {
        //
    }
}
