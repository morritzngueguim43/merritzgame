<?php

namespace App\Http\Controllers;

use App\Models\Bet_m;
use Illuminate\Http\Request;

class Bet_mController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bet_m  $bet_m
     * @return \Illuminate\Http\Response
     */
    public function show(Bet_m $bet_m)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bet_m  $bet_m
     * @return \Illuminate\Http\Response
     */
    public function edit(Bet_m $bet_m)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bet_m  $bet_m
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bet_m $bet_m)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bet_m  $bet_m
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bet_m $bet_m)
    {
        //
    }
}
