<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $fillable = [
        'login','password'
    ];
    
    /**
     * Get the profil associated with the user.
     */
    public function profil()
    {
        return $this->hasOne(Profil::class, 'id_user');
    }
}
