<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profils', function (Blueprint $table) {
            $table->id()->comment('la clé primaire id_profil');
            $table->string('nom')->comment('le nom du joueur');
            $table->string('prenom')->comment('le prenom du joueur');
            $table->string('email')->unique()->comment('un email unique du joueur');
            $table->char('sexe',5)->comment('le sexe Mr ou Ms');
            $table->timestamps();
            $table->foreignId('id_user')
                ->unique()->nullable()
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profils');
    }
};
