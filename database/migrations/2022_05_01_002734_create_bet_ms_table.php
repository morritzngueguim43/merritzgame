<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bet_ms', function (Blueprint $table) {
            $table->id()->comment('la clé primaire de bet_m');
            $table->double('mise')->comment('la valeur a parier par le joueur *');
            $table->string('choice')->comment('le choix du joueur sur lequel porte la mise');
            $table->foreignId('id_pari')->nullable()->constrained('paris')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('id_user')->nullable()->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bet_ms');
    }
};
