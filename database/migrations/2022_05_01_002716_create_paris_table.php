<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paris', function (Blueprint $table) {
            $table->id()->comment('la clé primaire de pari');
            $table->foreignId('id_joueur1')->nullable()->constrained('joueurs')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('id_joueur2')->nullable()->constrained('joueurs')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paris');
    }
};
