<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats', function (Blueprint $table) {
            $table->id()->comment('la clé primaire de stats');
            $table->bigInteger('n_match_t')->default(0)->comment('le nombre total de match joué');
            $table->bigInteger('n_match_g')->default(0)->comment('le nombre total de match gagné');
            $table->bigInteger('n_match_p')->default(0)->comment('le nombre total de match perdu');
            $table->double('max_earn')->default(0)->comment('le max de gains (total de tout les gains)');
            $table->double('max_lost')->default(0)->comment('le max de pertes (total de toutes les pertes)');
            $table->foreignId('id_user')
                ->unique()->nullable()
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stats');
    }
};
