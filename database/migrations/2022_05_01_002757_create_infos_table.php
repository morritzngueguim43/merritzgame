<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos', function (Blueprint $table) {
            $table->id()->comment('la clé primaire de info');
            $table->date('date_m')->comment('la date du match avec l\'heure de preference');
            $table->string('status')->default('WAITING')->comment('le status du match, PLAYING,WAITING,FINISHED');
            $table->foreignId('id_pari')
                ->unique()->nullable()
                ->constrained('paris')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos');
    }
};
