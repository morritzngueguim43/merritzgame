<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Profil>
 */
class ProfilFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nom'=>$this->faker->firstName,
            'prenom'=>$this->faker->lastName,
            'email'=>$this->faker->unique()->email,
            'sexe'=>Arr::random(['M','F']),
            'id_user'=>$this->faker->unique()->numberBetween(1,20)
        ];
    }
}
