<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Stats>
 */
class StatsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'n_match_t'=>$this->faker->randomFloat,
            'n_match_g'=>$this->faker->randomFloat,
            'n_match_p'=>$this->faker->randomFloat,
            'max_earn'=>$this->faker->randomFloat,
            'max_lost'=>$this->faker->randomFloat
        ];
    }
}
