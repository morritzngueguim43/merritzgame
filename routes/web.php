<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\StatsController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\PariController;
use App\Http\Controllers\JoueurController;
use App\Http\Controllers\InfoController;
use App\Http\Controllers\Bet_mController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource("users",UserController::class);
Route::resource("stats",StatsController::class);
Route::resource("profils",ProfilController::class);
Route::resource("paris",PariController::class);
Route::resource("joueurs",JoueurController::class);
Route::resource("infos",InfoController::class);
Route::resource("bet_ms",Bet_mController::class);

Route::get('/', function () {
    return view('welcome');
});
