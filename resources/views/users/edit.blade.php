@extends('layouts.users')

@section('title')

@section('container')
    <div class="form">
        <form action="{{route('users.update',['user'=>$user])}}" method="POST">
            @csrf
            @method('PUT')
            <input type="text" name="login" id="login" value="{{$user->login}}">
            <label for="login">Login</label><br>
            <input type="text" name="password" id="password" value="{{$user->password}}">
            <label for="password">password</label><br>
            <span>
                <button type="submit">Update</button>
                <button type="reset">Reset</button>
            </span>
        </form>
    </div>
@endsection