@extends('layouts.users')

@section('title')
    
@endsection

@section('container')
    <div class="container">
        @forelse ($users as $user)
            <h3>
                <a href="{{ route('users.show',['user'=>$user->id])}}">{{ $user->login}}</a>
                <a href="{{route('users.edit',['user'=>$user->id])}}">Edit</a>
                <form action="{{route('users.destroy',['user'=>$user->id])}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Delete</button>
                </form>
            </h3>
        @empty
            <h3>No Merritz user found !!!</h3>
        @endforelse
    </div> 
    {{ $users->links() }}
@endsection

 