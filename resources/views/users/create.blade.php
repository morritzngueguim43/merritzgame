@extends('layouts.users')

@section('title')

@section('container')
    <div class="form">
        <form action="{{route('users.store')}}" method="POST">
            @csrf
            <input type="text" name="login" id="login">
            <label for="login">Login</label><br>
            <input type="text" name="password" id="password">
            <label for="password">password</label><br>
            <span>
                <button type="submit">Send</button>
                <button type="reset">Reset</button>
            </span>
        </form>
    </div>
@endsection