@extends('layouts.stats')

@section('title')
    
@endsection

@section('container')
    <div class="container">
        @forelse ($stats as $stat)
            <h3>
                <a href="{{ route('stats.show',['stat'=>$stat->id])}}">{{ $stat->login}}</a>
                <a href="{{route('stats.edit',['stat'=>$stat->id])}}">Edit</a>
                <form action="{{route('stats.destroy',['stat'=>$stat->id])}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Delete</button>
                </form>
            </h3>
        @empty
            <h3>No Merritz stat found !!!</h3>
        @endforelse
    </div> 
    {{ $stats->links() }}
@endsection

 