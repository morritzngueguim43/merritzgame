@extends('layouts.stats')

@section('title')

@section('container')
    <div class="form">
        <form action="{{route('stats.update',['stat'=>$stat])}}" method="POST">
            @csrf
            @method('PUT')
            <input type="text" name="login" id="login" value="{{$stat->login}}">
            <label for="login">Login</label><br>
            <input type="text" name="password" id="password" value="{{$stat->password}}">
            <label for="password">password</label><br>
            <span>
                <button type="submit">Update</button>
                <button type="reset">Reset</button>
            </span>
        </form>
    </div>
@endsection