@extends('layouts.profils')

@section('title')
    
@endsection

@section('container')
    <div class="container">
        @forelse ($profils as $profil)
            <h3>
                <a href="{{ route('profils.show',['profil'=>$profil->id])}}">{{ $profil->nom}}</a>
                <a href="{{route('profils.edit',['profil'=>$profil->id])}}">Edit</a>
                <form action="{{route('profils.destroy',['profil'=>$profil->id])}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Delete</button>
                </form>
            </h3>
        @empty
            <h3>No Merritz profil found !!!</h3>
        @endforelse
    </div> 
    {{ $profils->links() }}
@endsection

 