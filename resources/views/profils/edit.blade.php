@extends('layouts.profils')

@section('title')

@section('container')
    <div class="form">
        <form action="{{route('profils.update',['profil'=>$profil])}}" method="POST">
            @csrf
            @method('PUT')
            
            <span>
                <button type="submit">Update</button>
                <button type="reset">Reset</button>
            </span>
        </form>
    </div>
@endsection